// Contains all the endpoints for our application

// We need to use express Router() function
const express = require("express");


// Creates a Router instance that functions as a middleware and routing system

// Allows access to HTTP method middlewares taht makes it easier to create routes for our application.
const router = express.Router();

const taskController = require("../controllers/taskController");

/*
	Syntax : localhost:3003/tasks
*/

// [SECTION] Routes

// Route to get akk the task
router.get("/get-info", (req, res) =>{

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));


})

// Route to create a new task
router.post("/create-info", (req, res) =>{

	// If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})


// Route to delete a task
// The colon (:) is an identifier that helps create dynamic route which allows us to supply information in the URL

// The word that comes after the colon(:) symbol will be the name of the URL parameter

// ":id" is a wildcard where you can put any value , it then creates a link between "id" parameter an the URL value provided in the URL

/*
	Example:
		localhost:3001/una/del/1234
		 the 1234 is assigned to the "id" parameter in the URL
*/

router.delete("/del/:id", (req,res)=>{
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})

// Route to update a task

router.put("/update/:id", (req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})


// ACTIVITY ------------------------------------------

router.put("/updaterStat/:id", (req, res)=>{
	taskController.updateStat(req.params.id, req.body).then(result => res.send(result));
})


router.get("/getSpecific/:id", (req, res) =>{

	taskController.specific(req.params.id).then(result => res.send(result));


})

// Use "module.exports" to export the router object to use in the "app.js"

module.exports = router;