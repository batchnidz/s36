// Initialization, Connection
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();

const taskRoute = require("./routes/taskRoute")

const app = express();
const port = 3004;
app.use(express.json());
app.use(express.urlencoded({ extended: true }))

mongoose.connect(`mongodb+srv://nidzzone:${process.env.PASSWORD}@cluster0.u2ylc4z.mongodb.net/MRC?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB."));

// Add the task routes
// Allows all the task routes created in the "taskRoute" file to use "/tasks" route

app.use("/una", taskRoute);

// Port connect
app.listen(port, () => console.log(`Server running at localhost: ${port}`));