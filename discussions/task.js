// Create the Schema, model and export the file

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	name : String,
	status :{
		type : String,
		default : "pending"
	}

})

// "module.exports" is the way for Node JS to treat this value as "package" that can be used by the other files.

module.exports = mongoose.model("Task", taskSchema);